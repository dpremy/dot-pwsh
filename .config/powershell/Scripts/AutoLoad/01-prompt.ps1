function global:prompt() {
  # Save last commands error state
  $LastCommandError = $?

  # Detect admin or root rights
  If ($PSVersionTable.PSEdition -eq 'Desktop' -or $IsWindows) {
    If (([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) {
      $IsAdmin = $true
    }
  }
  ElseIf ($PSVersionTable.PSEdition -eq 'Core' -and $IsLinux) {
    If ((id -u) -eq 0) {
      $IsAdmin = $true
    }
  }

  # Detect OS version and create string to show in prompt
  If ($IsWindows) {
      $OS = "Win "
  }
  ElseIf ($IsLinux) {
      $OS = "Lin "
  }

  # Start prompt with `(green or red)username`
  If ($IsAdmin) {
    Write-Host "$([Environment]::UserName)" -nonewline -foregroundcolor red
  }
  Else {
    Write-Host "$([Environment]::UserName)" -nonewline -foregroundcolor green
  }

  # Followed by `(blue)@(cyan)computer `
  Write-Host "@" -nonewline -foregroundcolor blue
  Write-Host "$([Environment]::MachineName)" -nonewline -foregroundcolor cyan

  # Followed by orange "(<OS> PS#)"
  Write-Host (" (" + $OS + "PS" + $PSVersionTable.PSVersion.Major + ") ") -nonewline -foregroundcolor yellow

  # Followed by a shortened `Path`
  If (((Get-Item $pwd).parent.parent.parent.parent.name)) {
    $Path = '..\' + (Get-Item $pwd).parent.parent.parent.name + '\' + (Get-Item $pwd).parent.parent.name + '\' + (Get-Item $pwd).parent.name + '\' + (Split-Path $pwd -Leaf)
  }
  Else {
    $Path = $pwd.path
  }
  Write-Host ($Path + " ") -nonewline -foregroundcolor yellow

  # Followed by colored green or red `Time`
  If ($LastCommandError) {
    $(Write-Host $(Get-Date).Tostring("HH:mm:ss") -foregroundcolor green)
  } Else {
    $(Write-Host $(Get-Date).Tostring("HH:mm:ss") -foregroundcolor red)
  }

  # Followed by a proper role based prompt
  If ($IsAdmin) {
    Write-Host $('$' * ($nestedPromptLevel + 1)) -nonewline
  }
  Else {
    Write-Host $('>' * ($nestedPromptLevel + 1)) -nonewline
  }

  Return " "
}
