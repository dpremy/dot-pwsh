# init zoxide
  $oldErrorPreference = $ErrorActionPreference
  $ErrorActionPreference = 'stop'
  Try {
    If (Get-Command zoxide) {
      Invoke-Expression (& { (zoxide init --cmd cd powershell | Out-String) })
    }
  }
  Catch {}
  Finally {$ErrorActionPreference=$oldErrorPreference}
