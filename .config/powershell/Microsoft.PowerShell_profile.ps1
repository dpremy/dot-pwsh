# Source all numbered 'AutoLoad' files
If (Test-Path -Path $($(Split-Path $Profile) + "\Scripts\AutoLoad\") -PathType Container) {
  Get-ChildItem $($(Split-Path $Profile) + "\Scripts\AutoLoad\[0-9]*.ps1") | ForEach-Object { .$_ }
}

# Run any passed parameters to load further scripts
$Params = 0
While ($Params -lt $args.length) {
  If ( Test-Path $args[$Params] -PathType leaf ) {
    Invoke-Expression -command "& '$($args[$Params])'"
  }
  $Params++
}
Remove-Variable -name Params
