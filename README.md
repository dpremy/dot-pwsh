# dpremy pwsh config files and shell aliases

![Build Status](https://ci.davidremy.me/api/badges/dpremy/dot-bash/status.svg)

## Purpose

This repository contains the dotfiles and configs I use with pwsh in. It is designed to work with [GNU stow](https://www.gnu.org/software/stow/), but can easily be used without it.

## Installation

### Linux

```shell
# if you don't already have GNU stow, install stow for your OS

# clone this repo in to a .files directory
git clone -q https://gitlab.com/dpremy/dot-pwsh.git ~/.files/dot-pwsh

# use stow to symlink this 'package' in to your home directory
stow -d ~/.files/ -t ~/ -S dot-pwsh
```

### Windows

```powershell
# open explorer to your PS Profile folder
explorer $(Split-Path $Profile)

# copy the contents of this repos .config/powershell directory to your profile

# unblock files to remove the Mark of the Web
Unblock-File -Path $Profile\*
Unblock-File -Path $Profile\Scripts\AutoLoad\*
```

## Usage

The files in [.config/powershell/](.config/powershell) may be worth reviewing.
